const swiper = new Swiper('.main-banner__inner .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
  
    // If we need pagination
    pagination: {
      el: '.main-banner__carousel__pagination',
    },
  
    // Navigation arrows
    navigation: {
      nextEl: '.main-banner__carousel__nav--next',
      prevEl: '.main-banner__carousel__nav--prev',
    }
});

document.querySelectorAll('.products-section').forEach(item => {
  const productCarousel = new Swiper(item.querySelector('.swiper-container'), {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    slidesPerView: 'auto',
    slidesPerGroup: 1,
    spaceBetween: 40,
  
    // If we need pagination
    pagination: {
      el: item.querySelector('.products-section__carousel__pagination'),
    },
  
    // Navigation arrows
    navigation: {
      nextEl: item.querySelector('.products-section__carousel__nav--next'),
      prevEl: item.querySelector('.products-section__carousel__nav--prev'),
    }
  });
});

document.querySelectorAll('.feedback').forEach(item => {
  const feedbacks = new Swiper(item.querySelector('.swiper-container'), {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    slidesPerView: 4,
    slidesPerGroup: 1,
    spaceBetween: 40,
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      // when window width is >= 480px
      768: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      // when window width is >= 640px
      1280: {
        slidesPerView: 3,
        spaceBetween: 40
      },
      1430: {
        slidesPerView: 4,
        spaceBetween: 40
      }
    },
  
    // If we need pagination
    pagination: {
      el: item.querySelector('.feedback__carousel__pagination'),
    },
  
    // Navigation arrows
    navigation: {
      nextEl: item.querySelector('.feedback__carousel__nav--next'),
      prevEl: item.querySelector('.feedback__carousel__nav--prev'),
    }
  });
});
