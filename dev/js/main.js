/* frameworks */
//=include ../../node_modules/jquery/dist/jquery.min.js
//=include ../../node_modules/swiper/js/swiper.min.js

/* libs */
//=include lib/modernizr-custom.js

/* plugins */
//=include ../../node_modules/popper.js/dist/umd/popper.min.js
//=include ../../node_modules/bootstrap/js/dist/util.js
//=include ../../node_modules/bootstrap/js/dist/dropdown.js
//=include ../../node_modules/bootstrap/js/dist/collapse.js
//=include ../../node_modules/select2/dist/js/select2.min.js

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
//=include components/js-header.js
//=include components/carousels.js

// the main code

$(document).ready(function() {
    $('.js-select').select2({
        minimumResultsForSearch: -1,
        placeholder: "Select country..."
    });
});